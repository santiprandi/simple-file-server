package main

import (
	"bufio"
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"
)

func readPort() string {
	scanner := bufio.NewScanner(os.Stdin)
	var port uint64
	var err error

	fmt.Print("Listening port (empty for 8080): ")
	for scanner.Scan() {
		if scanner.Text() == "" {
			return ""
		}
		port, err = strconv.ParseUint(scanner.Text(), 10, 16)
		if err != nil {
			fmt.Println("Invalid number, try again...")
			fmt.Print("Listening port (empty for 8080): ")
			continue
		}
		break
	}
	return strconv.FormatUint(port, 10)
}

func main() {
	var portStr string
	port := readPort()
	if port == "" {
		portStr = ":8080"
	} else {
		portStr = fmt.Sprintf(":%s", port)
	}

	fmt.Printf("Listening on port %s...", portStr)
	// Simple static webserver:
	log.Fatal(http.ListenAndServe(portStr, http.FileServer(http.Dir("."))))
}
